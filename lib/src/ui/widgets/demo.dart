import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preference_firebase/src/data/shared_pref.dart';
import 'package:shared_preference_firebase/src/model/message.dart';

//******************************** CLASS DEMO **********************************
class Demo extends StatefulWidget {
  //**********************
  @override
  DemoView createState() {
    return DemoView();
  }
}

//********************************* STATE DEMO *********************************
class DemoView extends State<Demo> {
  SharedPref sharedPref = SharedPref();
  MessageNotification notificationSave = MessageNotification();
  MessageNotification notificationLoad = MessageNotification();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  //**************************** METHOD LOAD SHARED PREF ***********************
  loadSharedPrefs() async {
    try {
      MessageNotification user =
      MessageNotification.fromJson(await sharedPref.read("message"));

      //:::::::::::::::::: SNACK BAR ::::::::::::::::::
      Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text("Loaded!"),
          duration: const Duration(milliseconds: 500)));

      //:::::::::::::::: SET STATE :::::::
      setState(() {
        notificationLoad = user;
      });
    } catch (Excepetion) {
      //:::::::::::::::::: SNACK BAR ::::::::::::::::::
      Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text("Nothing found!"),
          duration: const Duration(milliseconds: 500)));
    }
  }

  //******************************* INIT STATE *********************************
  @override
  void initState() {
    super.initState();

    //:::::::::::::::::::: GET ALL MESSAGING IN DATA BASE
    //notificationsFuture = ServiceNotification.getAllNotification(); //CALL CLASS
    loadSharedPrefs();

    //::::::::::::::::::::: CONFIGURE FIRE BASE MESSAGING ::::::::::::::
    _firebaseMessaging.configure(
      /*onBackgroundMessage: Theme.of(context).platform == TargetPlatform.iOS
          ? null
          : myBackgroundMessageHandler,  */

      //onBackgroundMessage:myBackgroundMessageHandler,

      //::::::::::::::::::::: IN
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");

        final notification = message['notification'];
        notificationSave.title = notification['title'];
        notificationSave.body = notification['body'];
        sharedPref.save("message", notificationSave);

        setState(() {
          notificationLoad = notificationSave;
        });
      },
      //::::::::::::::::::::::::: APP TERMINATED ::::::::::::::::::
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");

        final notification = message['notification'];

        notificationSave.title = notification['title'];
        notificationSave.body = notification['body'];
        sharedPref.save("message", notificationSave);

        setState(() {
          notificationLoad = notificationSave;
        });
      },

      //::::::::::::::::::::: APP IN THE BACKGROUND :::::::::::::::
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");

        final notification = message['notification'];
        notificationSave.title = notification['title'];
        notificationSave.body = notification['body'];
        sharedPref.save("message", notificationSave);

        setState(() {
          notificationLoad = notificationSave;
        });
      },
    );

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    //PERMISSION
    _firebaseMessaging
        .requestNotificationPermissions(const IosNotificationSettings(
        sound: true,
        badge: true, //active alert notification
        alert: true));

    //this create token for
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings setting) {
      print("IOS Setting Registed");
    });

    _firebaseMessaging.getToken().then((token) {
      update(token);
      //textValue = token;
      setState(() {});
    });
  }

  //******************************* PRINT TOKEN ********************************
  update(String token) {
    print(token);
  }

  //***************************** ROOT WIDGET DEMO *****************************

  //****************************** WIDGET MESSAGING ***********************
  Widget build(BuildContext context) {
    //:::::::::::::::::::::::::::: CARD BUILD ITEM ::::::::::::::::::::::::::::

    if(notificationLoad.title != null)
      return ListView(
        padding: const EdgeInsets.all(10),

        children: <Widget>[
          Card(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${notificationLoad.title}',
                    style: TextStyle(fontSize: 24),
                  ),
                  Text(
                    '${notificationLoad.body}',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 12),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        //onPressed: () =>  sharedPref.remove("message"),

                        onPressed: () {
                          sharedPref.remove("message");
                          setState(() {
                            notificationLoad = MessageNotification();
                          });
                        },
                        color: Colors.green,
                        child: Text('Apagar'),
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      );
    return Container();
  }
}
//******************************************************************************
