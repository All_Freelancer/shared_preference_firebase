import 'package:flutter/material.dart';
import 'package:shared_preference_firebase/src/ui/widgets/demo.dart';

//***************************** CLASS HOME PAGE ********************************
class MyHomePage extends StatefulWidget {

  //variable
  final String title;

  //***************************  CONSTRUCT *************************************
  MyHomePage({Key key, this.title}) : super(key: key);


  //************************
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

//**************************** STATE HOME PAGE *********************************
class _MyHomePageState extends State<MyHomePage> {

  //::::::::::::::::::: ROOT WIDGET HOME PAGE ::::::::::::::::::::::::::::::::
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Demo(),
    );
  }
}
//******************************************************************************
