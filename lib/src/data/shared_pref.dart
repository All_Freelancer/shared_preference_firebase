import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

//******************************* CLASS SHARED PREF ****************************
class SharedPref {

  //:::::::::::::::::::::::: READ SHARED ::::::::::::::::::::::::::::::
  read(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key));
  }

  //::::::::::::::::::::::::: SAVE SHARED :::::::::::::::::::::::::::::::
  save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  //::::::::::::::::::::::::::: REMOVE IN SHARED :::::::::::::::::::::::::
  remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }
}
//******************************************************************************
